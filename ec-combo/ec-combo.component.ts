import "./ec-combo.styles.less";
import {
    Component, ViewChild, Input, OnInit, EventEmitter, forwardRef, TemplateRef, ContentChild,
    ContentChildren, ElementRef, ChangeDetectorRef, AfterViewInit, NgZone
} from "@angular/core";
import {DataProvider, ArrayDataProvider} from "@echo/ui-common";
import {Output} from "@angular/core/src/metadata/directives";
import {ValueAccessor} from "../ValueAccessor";
import {EcPopupComponent} from "../ec-popup/ec-popup.component";

export enum EcComboTypes {
    SEARCH_SELECT = <any>'SEARCH_SELECT',
    SELECT = <any>'SELECT',
    SELECT_FILLED = <any>'SELECT_FILLED', // only for single select
    SEARCH = <any>'SEARCH'
}


@Component({})

export abstract class EcComboComponent extends ValueAccessor implements OnInit, AfterViewInit {

    types = EcComboTypes;

    @Input() type:any;

    @Input() dataProvider:DataProvider;

    @Input() placeholder:string;

    @Input() searchField:string;

    @Input() filter: any;

    @Output() onSelect = new EventEmitter <any> ();

    @ViewChild('inputWrap')
    protected inputWrap:any;

    @ViewChild('ulSuggest')
    protected ulSuggestElement:any;

    @ViewChild('popup')
    protected popup:EcPopupComponent;

    @ContentChild(TemplateRef)
    protected itemTemplate: TemplateRef<any>;

    protected _:any = _;

    protected isOpen:boolean = false;
    protected _isFocus:boolean = false;
    protected isSelectType:boolean;

    protected matchInputFilterOff:boolean = false;
    protected prevEmitValue:any;
    protected _inputValue:any;
    protected inputElement:JQuery;

    constructor(
        protected cd?: ChangeDetectorRef,
        protected zone?: NgZone
    ) { super() }

    abstract writeValue(value : any) : void;
    abstract selectItem(item : any) : void;
    abstract validateValue() : void;
    abstract comboFilter(item : any) : boolean;
    abstract onCustomType(keyCode:number) :void;

    ngOnInit(){
        this.isSelectType = this.type.indexOf('SELECT') == 0;
        this.inputWrap = $(this.inputWrap.nativeElement);
        this.ulSuggestElement = this.ulSuggestElement.nativeElement;
        this.filtersInit();
        this.dataProvider.load();
        if(this.isSelectType && !this.value){
            this.value = this.dataProvider.data[0];
        }
    }
    ngAfterViewInit(){
        this.inputElement = this.inputWrap.find('input');
    }
    set inputValue(value) {
        this._inputValue = value;
        this.dataProvider.load(); // filter init
    }
    get inputValue(){
        return this._inputValue;
    }
    set isFocus(value:boolean){
        value ? this.inputWrap.addClass('focus')
            : this.inputWrap.removeClass('focus');
        this._isFocus = value;
    }
    get isFocus(){
        return this._isFocus;
    }

    onFocus($event:Event) {
        $event.preventDefault();
        this.isFocus = true;
    }
    inputBlur() {
        this.isFocus = false;
        this.validateValue();
        this.isOpen = false;
        this.inputElement.blur();
    }
    open() {
        this.isFocus = true;
        this.isOpen = true;
        this.scrollCalcInit();
        this.popup.open();
    }
    emitSelectEvent(item:any) {
        this.onChangeCallback(this.value);
        this.onSelect.emit(item);
        this.prevEmitValue = _.isArray(this.value) ? this.value.slice()
            : this.value;
    }
    filtersInit() {
        this.dataProvider.filter = ( function (items) {
            // this data provider level own [ if smp going wrong just check this. ]
            if (this.filter) {
                items = items.filter(this.filter);
            }
            items = items.filter(this.comboFilter.bind(this));
            if(!this.inputValue) return items;
            if(this.type != this.types.SELECT
                && this.type != this.types.SELECT_FILLED
            ) {
                items = items.filter(this.matchInputFilter.bind(this));
            }
            return items;
        } ).bind(this);
    }
    private matchInputFilter(item : any){
        if (this.matchInputFilterOff) return true;

        if (item) {
            if (this.searchField) {
                return _.get(item, this.searchField)
                        .toString().search(new RegExp('.*' + this.inputValue + '.*', 'i')) == 0;
            } else {
                if (!item.search) console.warn('[ec-combo]: Template error, searchField is not defined');
                return item.search(new RegExp('.*' + this.inputValue + '.*', 'i')) == 0;
            }
        } else {
            return false;
        }
    }

    private buttonCount:number = 0;
    private scrollCalc:any;
    private scroll:number;

    buttonSelect($event:KeyboardEvent){
        if(!this.isOpen) this.open();
        if($event.keyCode == 38 || $event.keyCode == 40){
            const checkAndNext = (nextCount):number => {
                if( (nextCount <= this.dataProvider.data.length - 1) && (nextCount >= 0) ){
                    this.scrollCalc(nextCount);
                    return nextCount;
                } else {
                    return this.buttonCount;
                }
            };
            if($event.keyCode == 38){
                this.buttonCount = checkAndNext(this.buttonCount - 1);
            } else if($event.keyCode == 40){
                this.buttonCount = checkAndNext(this.buttonCount + 1);
            }
        } else if($event.keyCode == 13){
            if(this.buttonCount == -1) {
                this.selectItem(this.dataProvider.data[0]);
            } else {
                this.selectItem(this.dataProvider.data[this.buttonCount]);
            }
        } else if($event.keyCode == 27){
            this.popup.close();
            this.value = null;
        } else {
            this.onCustomType($event.keyCode);
        }
        this.cd.detectChanges();
    }

    scrollCalcInit():any {
        this.buttonCount = -1;
        this.scroll = 0;
        let ul = $(this.ulSuggestElement),
            li = $(this.ulSuggestElement.children[0]);

        const ulHeight = parseInt(ul.css('max-height')),
            liHeight =  parseInt(li.css('height'));
        let displayElsNumber = Math.round(ulHeight/liHeight),prevElNumber,
            viewRange = { start:1, end:displayElsNumber };

        this.scrollCalc = (nextCount)=>{
            let nextElNumber = nextCount + 1;
            if(nextElNumber > prevElNumber){
                if(nextElNumber > viewRange.end){
                    this.scroll += liHeight;
                    viewRange.start += 1; viewRange.end += 1;
                }
            } else {
                if(nextElNumber < viewRange.start){
                    this.scroll -= liHeight;
                    viewRange.start -= 1; viewRange.end -= 1;
                }
            }
            prevElNumber = nextElNumber;
        };
    }
    autoSetType(dataCount:number):EcComboTypes {
        let type:EcComboTypes;
        if(dataCount < 10){
            type = this.types.SELECT;
        } else if(dataCount < 40) {
            type = this.types.SEARCH_SELECT
        } else if(dataCount > 40) {
            type = this.types.SEARCH
        }
        return type;
    }
}
