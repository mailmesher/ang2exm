import {
    Component, ViewChild, TemplateRef, ContentChild, ChangeDetectorRef,
    NgZone, forwardRef, Input, ElementRef, OnDestroy
} from "@angular/core";
import {EcComboComponent} from "../ec-combo.component";
import {NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
    selector: 'ec-combo-m',
    templateUrl: 'ec-combo-m.template.html',
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => EcComboMultiComponent),
        multi: true
    }]
})

export class EcComboMultiComponent extends EcComboComponent
                                   implements OnDestroy {

    @Input('maxRows')
    private maxRows:number;

    @ViewChild('sizer')
    private sizer:any;

    @ContentChild(TemplateRef)
    public itemTemplate: TemplateRef<any>;

    private multiSelectState:any = {
        updateHeight: () =>
            this.multiSelectState.prevHeight = this.inputWrap.height(),
        prevHeight: 0, // замыкание для расчета высоты
        prevSumOfLetters: 0, // замыкание для сайзера инпута
        baseRowHeight: 30, /* базовая высота одной строки мультиинпута для
        расчета, при 9-10 строках возможны проблемы, хардкод связан с тем
        что тут значения this.multiContainer.height()
        в моменты ngAfterViewInit могут быть неточными */
        onInputFocusHeight:0,
        stopNextModeSwitch: false, // остановить следущее переключение режима
        shortMode: false, // state мультиселекста
        isInitPrevValue: false
    };

    private multiContainer:JQuery;

    private _inputVisible:boolean = false;
    get inputVisible(){
        return this._inputVisible;
    }
    set inputVisible(val:boolean){
        val ? this.inputElement.parent().css('display','inline-block')
            : this.inputElement.parent().hide();
        this._inputVisible = val;
    }


    constructor(
        cd: ChangeDetectorRef,
        zone: NgZone
    ) { super(cd, zone) }

    ngOnInit() {
        super.ngOnInit();
        if(!this.value) this.value = [];
    }
    ngAfterViewInit() {
        super.ngAfterViewInit();
        this.multiContainer = this.inputWrap.find('.multi-container');
        this.multiSelectState.updateHeight();
        this.setNativeHandlers(true);
    }
    ngOnDestroy(){
        this.setNativeHandlers(false);
    }
    writeValue(value:any[]) {
        if(!value) {
            value = [];
        } else if( !_.isArray(value) ){
            console.warn('[ec-combo-m] Error: Value is not an Array');
        }
        if (value !== this.innerValue) {
            this.value = value;

            if(!this.multiSelectState.isInitPrevValue) {
                this.prevEmitValue = value.length ? this.value.slice() : [];
                this.multiSelectState.isInitPrevValue = true;
            }
            if(value.length) {
                this.dataProvider.load();
                this.checkModeSwitch();
                this.calcPopupPosition();
            }
        }
    }
    validateValue() {
        this.inputValue = null;
        this.inputElement.val(null);
        this.inputVisible = false;
        this.multiSelectState.prevSumOfLetters = 0;
        this.calcInputWidth();
        this.checkModeSwitch( this.multiSelectState.onInputFocusHeight );
        this.multiSelectState.onInputFocusHeight = 0;
        this.cd.detectChanges();
    }
    selectItem(item:any) {
        this.multiSelectState.onInputFocusHeight = 0;
        this.value.push(item);
        this.checkModeSwitch();
        this.checkSumItems();
    }
    comboFilter(item:any): boolean {
        if (!this.value) return true;

        return !this.value.some( (valueItem : any) =>
            this.searchField ?
            _.get(valueItem, this.searchField) == _.get(item, this.searchField)
            : valueItem == item
        );
    }
    setNativeHandlers(isActive: boolean) {
        this.zone.runOutsideAngular( () => {
            if (isActive) {
                this.inputElement.on('keyup', this.buttonSelect.bind(this));
                this.inputElement.on('input', ($event:Event)=> {
                    this.calcInputWidth();
                    this.inputValue = $( $event.currentTarget ).val();
                    this.cd.detectChanges();
                });
            } else {
                this.inputElement.off('keyup');
                this.inputElement.off('input');
            }
        } );
    }
    removeItem(item:any, isPopupItemButton?:boolean):void {
        this.value = this.value.filter( (valueItem : any) =>
            this.searchField ?
            _.get(valueItem, this.searchField) != _.get(item, this.searchField)
                : valueItem != item
        );
        this.dataProvider.load();
        this.inputVisible = false;
        this.cd.detectChanges();
        this.multiSelectState.stopNextModeSwitch = true;
        this.multiSelectState.updateHeight();
        isPopupItemButton || this.emitSelectEvent(item);
    }
    checkSumItems(): void{
        this.dataProvider.load().then((items:any) => {
            if(items && items.length == 0) this.popup.close();
        });
        this.cd.detectChanges();
        this.calcPopupPosition();
    }
    calcInputWidth(): void {
        const sizer = $(this.sizer.nativeElement),
            baseInputWidth = 10;
        sizer.text(this.inputElement.val());
        this.inputElement.width(
            baseInputWidth + Math.round( sizer.outerWidth() )
        );
        this.calcPopupPosition();
    }
    onPopupClose(){
        this.checkModeSwitch();
        this.inputBlur();
        this.multiSelectState.updateHeight();
        if( this.isNewValueSelected(this.value, this.prevEmitValue) ){
            this.emitSelectEvent(this.value);
        }
    }
    onMultiContainerClick(){
        if( // случай когда клик идет по заполненному инпуту (без открытия окна)
            !this.isOpen && !this.multiSelectState.shortMode
            && this.value.length
        ){
            this.multiSelectState.onInputFocusHeight = this.multiContainer
                .height();
        }
        this.inputVisible = true;
        this.inputElement.focus();
        this.multiSelectState.updateHeight();
        this.checkModeSwitch();
    }
    onCustomType(keyCode:number): void {
        const isClearInput:boolean = !this.multiSelectState.prevSumOfLetters;
        if(keyCode == 8 && isClearInput) {
            this.value.pop();
            this.checkSumItems();
        }
        this.multiSelectState.prevSumOfLetters = this.inputValue ?
            this.inputValue.length : 0;
    }
    checkModeSwitch(initRealHeight?:number) {
        // если передаем initRealHeight то все вычисления на открытие и на
        // закрытие, будут выполнены относительно этой высоты
        if(this.multiSelectState.stopNextModeSwitch) {
            this.multiSelectState.stopNextModeSwitch = false;
            return;
        }
        if(!this.maxRows) return;
        this.cd.detectChanges();
        const allowHeight:number = this.maxRows *
            this.multiSelectState.baseRowHeight + 2;

        let realHeight:number = 0;
        if(!this.multiSelectState.shortMode){
            realHeight = initRealHeight ? initRealHeight
                : this.multiContainer.height();
        } else {
            realHeight = initRealHeight ? initRealHeight
                : $(this.popup.popupLayout).find('.multi-container-popup')
                .height();
        }
        if(allowHeight < realHeight) {
            this.multiSelectState.shortMode = true;
        } else if(allowHeight >= realHeight && !this.inputVisible) {
            this.multiSelectState.shortMode = false;
        }
        this.cd.detectChanges();
    }
    isNewValueSelected(newValue:any[], prevValue:any[]):boolean {
        const customSort = (a:any, b:any) => {
            if ( _.get(a, this.searchField) > _.get(b, this.searchField) ) {
                return 1;
            }
            if ( _.get(a, this.searchField) < _.get(b, this.searchField) ) {
                return -1;
            }
            return 0;
        };
        const isPreciselyDifferentValue =
            ( !newValue && prevValue ) ||
            ( newValue && !prevValue ) ||
            ( newValue.length != prevValue.length );
        if( !isPreciselyDifferentValue ) {
            return !_.isEqual(
                this.searchField ? newValue.sort(customSort)
                    : newValue.slice().sort(),
                this.searchField ? prevValue.sort(customSort)
                    : prevValue.slice().sort(),
            );
        }
        return true;
    }
    calcPopupPosition(): void {
        const inputWrapHeight = this.inputWrap.height();
        if(
            this.multiSelectState.prevHeight &&
            this.multiSelectState.prevHeight != inputWrapHeight
        ){
            this.checkModeSwitch();
        }
        this.popup.setPopupItemPoint();
        this.multiSelectState.prevHeight = inputWrapHeight;
    }
}
