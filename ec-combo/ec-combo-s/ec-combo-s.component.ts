import {Component, ChangeDetectorRef, NgZone, forwardRef} from "@angular/core";
import {EcComboComponent} from "../ec-combo.component";
import {NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
    selector: 'ec-combo-s',
    templateUrl: 'ec-combo-s.template.html',
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => EcComboSingleComponent),
        multi: true
    }]
})

export class EcComboSingleComponent extends EcComboComponent {

    constructor(
        cd: ChangeDetectorRef,
        zone: NgZone
    ) { super(cd, zone) }

    writeValue(value:any) {
        if (value !== this.innerValue) {
            if(value && this.searchField){
                this.inputValue = _.get(value, this.searchField);
            } else {
                this.inputValue = value
            }
            this.value = value;
        }
    }

    open(fromIconSelect?:boolean) {
        if(!this.dataProvider.data.length){
            this.value = null;
        }
        if(fromIconSelect){
            this.matchInputFilterOff = true;
            this.dataProvider.load().then( ()=>{
                this.matchInputFilterOff = false;
            });
        }
        super.open();
    }
    validateValue() {
        let setNull = ()=> {
            this.value = null;
            this.inputValue = null
        };
        if(this.value){
            if(this.searchField){
                if(_.get(this.value, this.searchField) != this.inputValue) {
                    setNull();
                }
            } else {
                if(this.value != this.inputValue) {
                    setNull();
                }
            }
        } else {
            if(this.dataProvider.data.length == 1){
                this.value = this.dataProvider.data[0];
                this.inputValue = this.value;
            } else {
                setNull();
            }
        }
        this.cd.detectChanges();
    }
    selectItem(item:any) {
        if(item){
            if(this.searchField) {
                this.inputValue = _.get(item, this.searchField);
            } else {
                this.inputValue = item;
            }
            this.value = item;
            this.popup.close();
        } else {
            this.inputValue = null;
            this.value = null;
        }
        this.emitSelectEvent(item);
    }
    comboFilter(item : any) : boolean {
        return true;
    }
    onCustomType(): void {
        this.value = null;
    }
}
