import "./ec-popup.styles.less";
import {
    Component, OnInit, forwardRef, Input, Output, EventEmitter, NgZone,
    ViewChild, ElementRef,
    AfterContentInit, ContentChild, OnDestroy, ChangeDetectorRef
} from "@angular/core";
import moment = require("moment");
import Moment = moment.Moment;


export class EcPopupConst {
    static containerId = 'popup-container';
}

@Component({
    selector: 'ec-popup',
    templateUrl: 'ec-popup.template.html'
})

export class EcPopupComponent implements AfterContentInit {

    @Input()
    ownerElement:HTMLElement;

    @Input()
    widthAsOwner:boolean;

    @Input()
    containerElement:HTMLElement;

    @Output()
    onClose = new EventEmitter <any> ();

    @Output()
    beforeClose = new EventEmitter <any> ();

    @ViewChild("wrapItemElement")
    wrapItemElement:ElementRef;


    popupLayout:Element;

    constructor(
        private zone:NgZone,
        private elementRef:ElementRef
    ){}

    ngAfterContentInit(): void {
        this.zone.runOutsideAngular(()=>{
            this.popupLayout = EcPopupComponent.appendPopupLayout(
                document.querySelector(`#${EcPopupConst.containerId}`)
            );
            $(this.popupLayout).hide(); // for hmr
        });
    }
    open(): void {
        this.zone.runOutsideAngular(()=>{
            const wrapItemFragment = document.createDocumentFragment();
            wrapItemFragment.appendChild(this.wrapItemElement.nativeElement);
            this.popupLayout.appendChild(wrapItemFragment);
            $(this.popupLayout).click( (event:UIEvent) => {
                if( $(event.target).attr('id') == this.popupLayout.id ) {
                    this.close(true);
                }
            });
            if(this.widthAsOwner) {
                $(this.wrapItemElement.nativeElement).width(
                    this.ownerElement.offsetWidth + 'px'
                );
            } else {
                $(this.wrapItemElement.nativeElement).width('auto');
            }
            $(this.popupLayout).show();
            this.setPopupItemPoint();
        });
    }
    close(isLayoutClose?: boolean): void {
        this.beforeClose.emit(isLayoutClose);
        this.zone.runOutsideAngular(()=>{
            $(this.popupLayout).hide();
            if( this.popupLayout.firstChild ) {
                this.popupLayout.removeChild(this.popupLayout.firstChild);
            }
            $(this.popupLayout).off('click');
        });
        this.onClose.emit(isLayoutClose);
    }
    setPopupItemPoint(options:PopupOptions = {}) {
        const container:JQuery = this.containerElement ? $(this.containerElement) : $(window),
            popupWrapItem:JQuery = $(this.wrapItemElement.nativeElement),
            popupItem:JQuery = popupWrapItem.children(),
            ownerRect:ClientRect = this.ownerElement.getBoundingClientRect();

        !options.direction && ( options.direction = EcPopupComponent.getPopupDirection(
            container, ownerRect, popupItem )
        );
        !options.position && ( options.position = EcPopupComponent.getPopupPosition(
            ownerRect, popupItem )
        );
        popupWrapItem.css({
            top: options.position[options.direction].y + 'px',
            left: options.position[options.direction].x + 'px'
        });
    }
    static setMirroringClass(verticalMirroring: boolean,
                      horizontalMirroring: boolean, popupItem: JQuery): void {
            const vmc = 'verticalMirroring', hmc = 'horizontalMirroring';
            verticalMirroring ? popupItem.addClass(vmc)
                : popupItem.removeClass(vmc);
            horizontalMirroring ? popupItem.addClass(hmc)
                : popupItem.removeClass(hmc);
    }
    static appendPopupLayout(popupContainer:Element): Element {
        if(!popupContainer) {
            popupContainer = document.createElement('div');
            popupContainer.id = EcPopupConst.containerId;
            document.body.appendChild(popupContainer);
        } else {
            if(popupContainer.firstChild) {
                popupContainer.removeChild(popupContainer.firstChild);
            }
        }
        return popupContainer;
    }
    static getPopupDirection(container:JQuery, ownerRect:ClientRect, popupItem:JQuery): string {
        let maxX = container.width() ,
            maxY = container.height();

        let verticalMirroring: boolean, horizontalMirroring: boolean;

        verticalMirroring = ownerRect.bottom + popupItem.outerHeight() > maxY;
        horizontalMirroring = ownerRect.left + popupItem.outerWidth() > maxX;

        this.setMirroringClass(verticalMirroring, horizontalMirroring, popupItem);
        return PopupDirection.getBy(verticalMirroring, horizontalMirroring);
    }
    static getPopupPosition( ownerRect:ClientRect, popupItem:JQuery ): PopupPosition {
        return {
            bottomRight: { x: ownerRect.left, y: ownerRect.bottom },
            bottomLeft: { x: ownerRect.right - popupItem.outerWidth(), y: ownerRect.bottom },
            topRight: { x: ownerRect.left, y: ownerRect.top - popupItem.outerHeight() },
            topLeft: { x: ownerRect.right - popupItem.outerWidth(), y: ownerRect.top - popupItem.outerHeight() }
        };
    };
}
export class PopupDirection {
    static bottomRight = 'bottomRight';
    static bottomLeft = 'bottomLeft';
    static topLeft = 'topLeft';
    static topRight = 'topRight';

    static getBy(verticalMirroring: boolean, horizontalMirroring: boolean) : string { //mirror
        return verticalMirroring ?
            (horizontalMirroring ? PopupDirection.topLeft : PopupDirection.topRight):
            (horizontalMirroring ? PopupDirection.bottomLeft : PopupDirection.bottomRight)
    }
}

export interface PopupPosition {
    bottomRight?: WebKitPoint;
    bottomLeft?: WebKitPoint;
    topLeft?: WebKitPoint;
    topRight?: WebKitPoint;
}

export interface PopupOptions {
    position?: PopupPosition;
    direction?: string;
    container?: JQuery;
}