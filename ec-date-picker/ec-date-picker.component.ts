import "./ec-date-picker.styles.less";
import {Component, AfterViewInit, ViewChild, Input, OnInit, EventEmitter,
    forwardRef, ElementRef, NgZone, ChangeDetectorRef
} from "@angular/core";
import {Output} from "@angular/core/src/metadata/directives";
import {ValueAccessor} from "../ValueAccessor";
import {NG_VALUE_ACCESSOR} from "@angular/forms";
import moment = require("moment");
import Moment = moment.Moment;
import {TimeFormatType} from "../../utils/enums/time.enums";
import {EchoUtils} from "../../utils/echo.utils";
import {lang} from "@echo/ui-core";

export enum EcDatePickerTypes {
    MULTIPLE = <any>'MULTIPLE',
    INTERVAL = <any>'INTERVAL',
    SINGLE = <any>'SINGLE'
}
export interface EcDatePickerOptions {
    datePickerOptions?: any,
    type?: EcDatePickerTypes,
    allowOneDayPick?: boolean,
    minutesSelect?: boolean,
    timePicker?: boolean
}


@Component({
    selector: 'ec-date-picker',
    templateUrl: 'ec-date-picker.template.html',
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => EcDatePickerComponent),
        multi: true
    }]
})
export class EcDatePickerComponent extends ValueAccessor
                                   implements OnInit, AfterViewInit {

    private timeFormat = TimeFormatType;
    private types = EcDatePickerTypes;

    @lang([require('./lang/ec-date-picker.lang.yaml')])
    lang: any;

    @Input('options')
    private options: EcDatePickerOptions = {};

    @Input('placeholder')
    private placeholder: string;

    @Output()
    private onSelect = new EventEmitter <any> ();

    @ViewChild('datePickerContainer')
    private datePickerContainer: ElementRef;

    @ViewChild('popup')
    private popup;


    private datePicker: any;
    private prevValues:Moment[];
    private preSetValue:Moment[];

    constructor(
        private elementRef:ElementRef,
        private zone: NgZone,
        private cd: ChangeDetectorRef
    ) {
        super()
    }

    ngOnInit(): void{
        const defaultOptions:EcDatePickerOptions = {
            datePickerOptions: {
                language: 'custom',
                autoClose: true,
                range: this.options.type ?
                       this.options.type == EcDatePickerTypes.INTERVAL : true,
                onHide: this.onDatePickerSelect.bind(this),
                onSelect: this.options.type == EcDatePickerTypes.MULTIPLE ?
                    this.onDatePickerMultiSelect.bind(this) : null,
                onShow: ()=> true, //stub
                toggleSelected: ( () => {
                    if(this.options.type == EcDatePickerTypes.MULTIPLE) {
                        return true;
                    } else {
                        return this.options.allowOneDayPick ?
                            !this.options.allowOneDayPick : false;
                    }
                } )()
            },
            type: EcDatePickerTypes.INTERVAL,
            allowOneDayPick: true,
            minutesSelect: true,
            timePicker: true
        };
        this.options = _.merge(defaultOptions, this.options);
    }
    ngAfterViewInit(): void{
        this.init();
    }
    writeValue(values: Moment[]): void {
        if(!(this.datePicker && values && values.length)) {
            this.innerValue = null;
            this.preSetValue = null;
            this.cd.detectChanges();
            return;
        }
        if( this.validateDates(values) ){
            // когда значение приходит с сервера, или выставляется
            // вне тайм пикера вызываем обычный селект
            this.value = values;
            this.prevValues = _.map(this.value, (date:Moment) => date);
            _.each(values, (v: Moment) =>
                this.datePicker.selectDate( v.toDate() ) );
            this.cd.detectChanges();
        }

    }
    init(): void {
        this.zone.runOutsideAngular(() => {
            const spl:string = ', ';
            $.fn.datepicker.language['custom'] = {
                days: this.lang['dp_days'].split(spl),
                daysShort: this.lang['dp_days_short'].split(spl),
                daysMin: this.lang['dp_days_min'].split(spl),
                months: this.lang['dp_months'].split(spl),
                monthsShort: this.lang['dp_months_short'].split(spl),
                today: this.lang['dp_today'],
                clear: this.lang['dp_clear'],
                firstDay: 1
            };
            this.datePicker = $(this.datePickerContainer.nativeElement)
                .datepicker(this.options.datePickerOptions)
                .data('datepicker');
        });
    }
    open(): void {
        // всегда устанавливаем значение в пикер при открытии, это позволит
        // избегать проблем с синхронизацией данных
        /* preSetValue это значение которым мы играем в popup*/
        if(this.value && this.value.length){
            this.preSetValue = _.map( this.value, (v:Moment) => v.clone() );
            _.each(
                this.preSetValue,
                (v:Moment) => this.datePicker.selectDate(v.toDate())
            );
        } else {
            this.datePicker.clear();
        }
        this.popup.open();
    }
    onClose(isLayoutClose: boolean): void {
        if(isLayoutClose) {
            this.value = this.prevValues;
        }
    }
    clear(): void {
        this.value = [];
        this.prevValues = null;
        this.preSetValue = null;
        this.onChangeCallback(this.value);
        this.onSelect.emit(this.value);
    }
    onDatePickerSelect(dp: any): void {
        this.preSetValue = _.map(
            dp.selectedDates,
            (date:Date) => moment(date)
        );
        if(
            this.options.type == EcDatePickerTypes.INTERVAL
            && this.options.allowOneDayPick
        ) {
            EcDatePickerComponent.setOneDayValues(this.preSetValue);
        }
        this.cd.detectChanges();
    }
    onDatePickerMultiSelect(dates:string) {
        let datesArray = dates.split(',');
        if(datesArray.length == this.options.datePickerOptions.multipleDates) {
            this.preSetValue = datesArray.map( (item: string) =>
                moment(item, "DD.MM.YYYY")
            );
        } else {
            this.preSetValue = [];
        }
        this.cd.detectChanges();
    }
    submitSelect(): void {
        this.popup.close();
        if( !this.validateDates(this.preSetValue) ) return;
        this.value = this.preSetValue;
        this.prevValues = _.map(this.value, (date:Moment) => date);
        this.cd.detectChanges();
        this.onChangeCallback(this.value);
        this.onSelect.emit(this.value);
    }
    validateDates(values: Moment[]): boolean {
        // отсеим кейсы
        if(
            ( values.length != 2
            && this.options.type == EcDatePickerTypes.INTERVAL )
            || ( values.length != 1
            && this.options.type == EcDatePickerTypes.SINGLE )
        ) {
            console.warn('[ec-date-picker] ERROR: Incorrect number of values');
            return false;
        }
        // когда выбрано недостаточно дат ( MULTIPLE )
        if(this.options.type == EcDatePickerTypes.MULTIPLE
            && values.length < this.options.datePickerOptions.multipleDates) {
            return false;
        }
        //когда даты не меняются
        return this.isNewDates(values);
    }

    setTime(isAddTime: boolean, value:Moment,
            count: number = 1 , unit: string = 'h'){

        EchoUtils.Time.setClockTime(isAddTime, count, unit, value);

        if(
            this.options.type == EcDatePickerTypes.INTERVAL
            && this.preSetValue[1].isBefore(this.preSetValue[0])
        ) {
            EchoUtils.Time.setClockTime(!isAddTime, count, unit, value);
        }
        this.cd.detectChanges();
    }
    static setOneDayValues(values:Moment[]): void {
        if(
            values.length == 2 &&
            !values[0].diff(values[1])
        ){
            values[0] = values[0].startOf('day');
            values[1] = values[1].endOf('day');
        }
    }

    isNewDates(values:Moment[]): boolean {
        let isDifferentDates = false;
        _.each(values, (v:Moment, i:number)=>{
            if(!this.prevValues || !this.prevValues[i]) {
                isDifferentDates = true;
                return;
            }
            if(this.prevValues[i].toDate().getTime() != v.toDate().getTime()) {
                isDifferentDates = true;
            }
        });
        return isDifferentDates;
    }
    getValueAsString(): string {
        if(!this.value || !this.value.length) return '';

        if(this.value.length > 1) {
            const isSameDay = this.value[0].isSame(this.value[1], 'day');
            if(isSameDay){
                return this.value[0].format(TimeFormatType.PICKER_TITLE_SHORT)
                      + " (" + this.value[0].format(TimeFormatType.ONLY_TIME)
                      + " - " + this.value[1].format(TimeFormatType.ONLY_TIME)
                      + ")";
            } else {
                return this.value[0].format(TimeFormatType.PICKER_TITLE) + ' - '
                    + this.value[1].format(TimeFormatType.PICKER_TITLE);
            }
        } else {
            return this.value[0].format(TimeFormatType.PICKER_TITLE) + " (" +
                this.value[0].format(TimeFormatType.ONLY_TIME) + ")" ;
        }
    }
}
