import "./ec-slicer.styles.less";
import {Component, OnInit, forwardRef, Input, Output, EventEmitter, NgZone, ViewChild, ElementRef} from "@angular/core";
import {ValueAccessor} from "../ValueAccessor";
import {NG_VALUE_ACCESSOR} from "@angular/forms";
import {EchoUtils} from "../../utils/echo.utils";
import moment = require("moment");
import Moment = moment.Moment;


export class EcSlicerTypes {
    static value = {
        DATE:'DATE'
    };
    static control = {
        RANGE:'RANGE'
    }
}

@Component({
    selector: 'ec-slicer',
    templateUrl: 'ec-slicer.template.html',
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => EcSlicerComponent),
        multi: true
    }]
})

export class EcSlicerComponent extends ValueAccessor implements OnInit {

    @ViewChild('slicer')
    slicerElement:ElementRef;

    @Input('valueType') valueType: any;

    @Output() onSlice = new EventEmitter <any> ();

    private slicer:any;
    private moments:Moment[];

    static defaultOptions = {
        DATE:{
            start: [0, 100],
            range: {'min': 0, 'max': 100},
            connect: true,
            tooltips: true,
            step: 10,
            format: {
                to: (value:number)=> {
                    value = Math.round(value);
                    return EchoUtils.Time.convertTo_00_00_Format(value);
                },
                from:(value:number) => Math.round(value)
            }
        }
    };


    constructor(private zone: NgZone) {
        super();
    }

    ngOnInit(): void {
        this.slicer = this.slicerElement.nativeElement;
    }
    createSlicer(options?: any, valueType?: string){
        this.zone.runOutsideAngular(() => {
            noUiSlider.create(this.slicer, options ?
                _.merge(EcSlicerComponent.defaultOptions[valueType] ,options) :
                EcSlicerComponent.defaultOptions[valueType]
            );
            this.slicer.noUiSlider.on('change', this.onSliceEmit.bind(this));
        });
    }
    onSliceEmit(formatted, handle, values){
        let from = this.moments[0].clone().add(values[0], 'minute'),
            to = this.moments[0].clone().add(values[1], 'minute'),
            timeError = this.moments[1].diff(to);
        if(timeError == -1) to.add(timeError); // error when even/odd correction
        this.zone.run( () => this.onSlice.emit([from, to]) );
    }

    init(range:Moment[], startValues?:Moment[]){
        this.moments = range;
        if(range){
            if(this.slicer.noUiSlider) this.slicer.noUiSlider.destroy();
            let minutes = range[1].diff(range[0], 'minute'),
                maxValue = EchoUtils.Number.isOdd(minutes) ? minutes + 1 : minutes;
            this.createSlicer({
                start: !startValues ? [0, maxValue] : (():number[] => {
                    let from = startValues[0].diff(range[0], 'minute'),
                        to = startValues[1].diff(range[0], 'minute');
                    return [from, to];
                })(),
                range: {'min': 0, 'max': maxValue}
            }, EcSlicerTypes.value.DATE);
        } else {
            this.slicer.noUiSlider.reset();
        }
    }
}