import "./time-filter.styles.less";
import {Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef} from "@angular/core";
import {ArrayDataProvider} from "@echo/ui-common";
import {EcDatePickerTypes} from "../../ec-date-picker/ec-date-picker.component";
import {ItemTranslateModel} from "../../../models/itemTranslate.model";
import {DataFilterComponent} from "../data-filter.component";
import {RangeValueModel} from "../../../models/filterValue/rangeValue.model";
import {FrameValueModel, FrameTypes} from "../../../models/filterValue/frameValue.model";
import {
    OperatorType,
    SingleValueModel
} from "../../../models/filterValue/singleValue.model";
import {ValueConstants} from "../../../models/filter.model";
import {EcSlicerTypes, EcSlicerComponent} from "../../ec-slicer/ec-slicer.component";
import moment = require('moment');
import Moment = moment.Moment;


export class TimePickerTypes {
    static AFTER = 'AFTER';
    static BEFORE = 'BEFORE';
    static CUSTOM = 'CUSTOM';
    static FRAME = 'FRAME';
}

@Component({
    selector: 'time-filter',
    templateUrl: 'time-filter.template.html'
})
export class TimeFilterComponent extends DataFilterComponent implements OnInit, AfterViewInit {

    @ViewChild(EcSlicerComponent)
    private customSlicer:EcSlicerComponent;

    EcDatePickerTypes = EcDatePickerTypes;
    TimePickerTypes = TimePickerTypes;
    EcSlicerTypes = EcSlicerTypes;

    mainComboValue:SingleValueModel;
    selectedFilterType:any;
    frameTypesProvider = new ArrayDataProvider([
        new ItemTranslateModel(FrameTypes.DAY),
        new ItemTranslateModel(FrameTypes.WEEK),
        new ItemTranslateModel(FrameTypes.MONTH)
    ]);
    framesFromPicker:Moment[];

    private slicerMode:boolean;

    constructor(private cd: ChangeDetectorRef){
        super();
    }

    ngOnInit(){
        super.ngOnInit();
        this.setMainComboValue();
        this.setDefaultValues();
    }
    ngAfterViewInit(): void {
        if(this.filterInstance.value instanceof RangeValueModel){
            this.checkSlicerMode(this.filterInstance.value.arrayRep);
            this.cd.detectChanges();
            // if(this.slicerMode){
            //     this.customSlicer.init(
            //         [
            //             this.filterInstance.value.arrayRep[0].clone().startOf('day'),
            //             this.filterInstance.value.arrayRep[0].clone().endOf('day')
            //         ]
            //         ,this.filterInstance.value.arrayRep);
            // }
        }
    }

    selectMainCombo(pickerItem:ItemTranslateModel):void {
        let newMainComboValue = new SingleValueModel({
            value: pickerItem ? pickerItem : null
        });
        if(pickerItem && pickerItem.value == TimePickerTypes.CUSTOM) {
            this.filterInstance.fromData = false;
            this.filterInstance.value = new RangeValueModel({fromData:false});
            this.filterInstance.valueType = ValueConstants.types.RANGE;
            /*  в этот момент происходит биндинг на value поэтому все данные
             в this.filterInstance.value должны быть готовы */
            this.selectedFilterType = TimePickerTypes.CUSTOM;
            this.checkSlicerMode(pickerItem);
        } else if(pickerItem && pickerItem.value == TimePickerTypes.FRAME) {
            this.filterInstance.fromData = false;
            this.filterInstance.value = new FrameValueModel({
                frames:[{}, {}],
                temporal: FrameTypes.DAY
            });
            this.filterInstance.valueType = ValueConstants.types.FRAME;
            this.selectedFilterType = TimePickerTypes.FRAME;
            this.checkSlicerMode(pickerItem);
        } else if(
            pickerItem && ( pickerItem.value == TimePickerTypes.AFTER
            || pickerItem.value == TimePickerTypes.BEFORE )
        ){
            this.filterInstance.fromData = false;
            this.filterInstance.valueType = ValueConstants.types.SINGLE;
            this.selectedFilterType = pickerItem.value;
            this.filterInstance.value = new SingleValueModel(
                {
                    operator: pickerItem.value == TimePickerTypes.AFTER ?
                        OperatorType.GREATER : OperatorType.LESS,
                    value: null
                }
            );
            this.cd.detectChanges();
        } else {
            this.filterInstance.fromData = true;
            this.filterInstance.value = newMainComboValue;
            this.filterInstance.valueType = ValueConstants.types.SINGLE;
            this.selectedFilterType = null;
            this.onSelectEmit(pickerItem);
        }
    }
    selectCustom(custom:Moment[]):void {
        this.onSelectEmit(custom);
        if(this.slicerMode) {
            // this.customSlicer.init(custom);
        }
    }

    selectFrameType(pickerItem:ItemTranslateModel):void {
        if(this.filterInstance.value.validateFramesData()) {
            this.onSelectEmit(pickerItem);
        }
    }
    selectFrame(dates:Moment[]):void {
        this.filterInstance.value.frames[0].moments[0] = dates[0];
        this.filterInstance.value.frames[1].moments[0] = dates[1];
        if(this.filterInstance.value.validateFramesData()) {
            this.onSelectEmit(dates);
        }
    }

    onSelectEmit(data:Moment[]|ItemTranslateModel):void {
        this.checkSlicerMode(data);
        this.onSelect.emit(data);
    }

    checkSlicerMode(data:Moment[]|ItemTranslateModel):boolean {
        let slicerMode = (()=>{
            if(!data || !( _.isArray(data) && data.length ) ) return false;
            if(
                ( data instanceof ItemTranslateModel && data.value == 'ONE_DAY' ) ||
                ( _.isArray(data) && !data[0].diff(data[1], 'days') ) ||
                ( this.filterInstance.value instanceof FrameValueModel &&
                this.filterInstance.value.temporal.value == FrameTypes.DAY )
            ) {
                return true;
            } else {
                return false;
            }
        })();
        this.slicerMode = slicerMode;
        return slicerMode;
    }

    sliceTime(value:Moment[]){
        if(this.filterInstance.value instanceof RangeValueModel) {
            this.filterInstance.value.arrayRep = value;
            this.onSelect.emit(value);
        }
    }

    setMainComboValue() {
        if(this.filterInstance.value instanceof SingleValueModel) {
            if(this.filterInstance.fromData) { // pre define values
                this.mainComboValue = new SingleValueModel({
                    value: this.filterInstance.value.singleValue ?
                        new ItemTranslateModel(
                            this.filterInstance.value.singleValue
                        ) : null
                });
            } else { // data values
                this.selectedFilterType = TimePickerTypes.AFTER;
                this.mainComboValue = new SingleValueModel({
                    value:
                        this.filterInstance.value.operator == OperatorType.LESS
                        ? new ItemTranslateModel(TimePickerTypes.BEFORE)
                        : new ItemTranslateModel(TimePickerTypes.AFTER)
                });
                this.filterInstance.value.singleValue =
                    this.filterInstance.value.singleValue ?
                    [moment(this.filterInstance.value.singleValue)] : null;
            }
        } else if(this.filterInstance.value instanceof RangeValueModel) {
            this.selectedFilterType = TimePickerTypes.CUSTOM;
            this.mainComboValue = new SingleValueModel({
                value: new ItemTranslateModel(TimePickerTypes.CUSTOM)
            });
        } else if(this.filterInstance.value instanceof FrameValueModel) {
            this.selectedFilterType = TimePickerTypes.FRAME;
            this.mainComboValue = new SingleValueModel({
                value: new ItemTranslateModel(TimePickerTypes.FRAME)
            });
        } else if(!this.filterInstance.value){
            this.mainComboValue = new SingleValueModel({value: null});
        }
    }

    setDefaultValues() {
        if(this.filterInstance.value instanceof FrameValueModel) {
            let isFrameSelected = this.filterInstance.value.frames[0].moments[0];
            if(isFrameSelected) {
                this.framesFromPicker = [
                    this.filterInstance.value.frames[0].moments[0],
                    this.filterInstance.value.frames[1].moments[0]
                ];
            }
        }
    }
}