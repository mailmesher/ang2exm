import {Component, OnInit, forwardRef, Input, Output, EventEmitter} from "@angular/core";
import {ValueAccessor} from "../ValueAccessor";
import {NG_VALUE_ACCESSOR} from "@angular/forms";
import {DataProvider, ArrayDataProvider} from "@echo/ui-common";
import {FilterModel, FilterTypes} from "../../models/filter.model";
import {EcComboComponent, EcComboTypes} from "../ec-combo/ec-combo.component";
import {lang} from "@echo/ui-core";
import {EcDatePickerTypes} from "../ec-date-picker/ec-date-picker.component";
import {ItemTranslateModel} from "../../models/itemTranslate.model";

@Component({
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => DataFilterComponent),
        multi: true
    }]
})
export class DataFilterComponent extends ValueAccessor implements OnInit {

    @lang([require('./lang/data-filter.lang.yaml')])
    lang: any;

    @Input()
    filterInstance:FilterModel;

    @Output()
    onSelect = new EventEmitter <any> ();

    dataProvider:DataProvider;
    comboType:EcComboTypes;

    filterTypes = FilterTypes;
    comboTypes = EcComboTypes;


    ngOnInit(): void {
        // todo split sync and async filters data for auto getComboType
        if(this.filterInstance.data) {
            this.dataProvider = new ArrayDataProvider(this.filterInstance.data);
            this.comboType = this.getComboType();
        }
    }

    getComboType():EcComboTypes {
        let dataCount:number = this.filterInstance.data.length, type:EcComboTypes;
        if(dataCount < 10){
            type = this.comboTypes.SELECT;
        } else if(dataCount < 40) {
            type = this.comboTypes.SEARCH_SELECT
        } else if(dataCount > 40) {
            type = this.comboTypes.SEARCH
        }
        if(this.filterInstance.mandatory){
            type = this.comboTypes.SELECT_FILLED
        }
        return type;
    }

    select(item, isDateCombo?:any):void {
        this.onSelect.emit(item);
    }
}