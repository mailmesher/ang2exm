import {Component, OnInit, Input} from "@angular/core";
import {DataFilterComponent} from "../data-filter.component";
import {ArrayDataProvider} from "@echo/ui-common";

@Component({
    selector: 'single-filter',
    templateUrl: 'single-filter.template.html',
})
export class SingleFilterComponent extends DataFilterComponent {
    @Input('maxRows')
    private maxRows:number;
}
