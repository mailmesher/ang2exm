/**
 * Created by meshcherin on 11.07.2017.
 *
 */
import './filters-panel.styles.less';
import {
    Component, OnInit, Input, Output, EventEmitter,
    ChangeDetectorRef, ViewChild, ElementRef
} from "@angular/core";
import {
    FilterModel, FilterTypes,
    ValueConstants
} from "../../../models/filter.model";
import {lang} from "@echo/ui-core";
import {DimensionModel} from "../../../models/olap/dimension.model";
import {FieldInfo} from "../../../models/filter/fieldInfo.model";
import {PageService} from "../../../services/page.service";
import {EcButtonComponent} from "@echo/ui-kit/components";

@Component({
    selector: 'filters-panel',
    templateUrl: 'filters-panel.template.html',
})
export class FiltersPanelComponent implements OnInit{

    FilterTypes = FilterTypes;

    @lang([require('../lang/data-filter.lang.yaml')])
    lang:any;

    @Input('filters')
    filters: FilterModel[];

    @Input('dimensions')
    dimensions: FieldInfo[];

    @Output()
    onFiltersChange = new EventEmitter<any>();

    @Output()
    onSaveFilters = new EventEmitter<FilterModel[]>();

    @ViewChild('saveButton')
    saveButton: EcButtonComponent;

    @ViewChild('cancelButton')
    cancelButton: EcButtonComponent;


    private isDetachInterface = false;

    private _editMode: boolean = false;
    set editMode(value: boolean) {
        this._editMode = value;
        if(value) {
            this.setEditFilters();
        } else {
            this.isDetachInterface = false;
            this.cd.reattach();
            this.cd.detectChanges();
        }
    }
    get editMode(): boolean {
        return this._editMode;
    }


    constructor(
        private cd:ChangeDetectorRef,
        private pageService: PageService
    ){}

    ngOnInit(): void {

    }
    ngAfterViewInit() {

    }

    isActiveFilter(dimensionName: string): boolean {
        return this.filters.some( (filter: FilterModel) =>
            dimensionName == filter.expression.value
        );
    }

    setEditFilters(): void {
        this.dimensions.forEach( (dimension: FieldInfo) => {
            dimension.isActive = this.isActiveFilter(dimension.name.value);
        });

    }

    filterSelect(item: any): void {
        this.onFiltersChange.emit(item);
    }

    saveFilters(): void {
        let newFilters:FilterModel[] = this.dimensions.filter(
        (d: FieldInfo) => d.isActive )
        .map( (d: FieldInfo) => {
            return new FilterModel({
                value: null,
                expression: d.name.value,
                fromData: d.type == FilterTypes.DATETIME.toString(),
                filterType: d.type,
                valueType: ValueConstants.types.SINGLE,
                data: null,
                mandatory: false
            });
        });
        this.saveButton.loading = true;
        this.cancelButton.setDisabled(true);

        this.pageService.saveFilters(newFilters).then(
            (newFilters: any[]) => {
                this.filters = _.map(newFilters, (filter: any) => {
                    return new FilterModel(filter);
                });
                this.onSaveFilters.emit(this.filters);
                this.saveButton.loading = false;
                this.cancelButton.setDisabled(false);
                this.editMode = false;
            }
        );
    }

    detachInterface(): void {
        if(this.isDetachInterface) return;
        this.isDetachInterface = true;
        this.cd.detach();
    }
}
